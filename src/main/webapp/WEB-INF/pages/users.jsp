<%--
  Created by IntelliJ IDEA.
  User: andrey
  Date: 17.11.16
  Time: 12.11
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<html>
<head>
    <title>User </title>

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 5px 5px;
            height: 27px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }

        .pagination p{
            border-collapse: collapse;
            border-spacing: 0;
            font-family: Arial, sans-serif;
            font-size: 16px;
            font-weight: normal;
            text-decoration: none;
            text-indent: 25px;

        }
        .pagination a{
            color: #191970;
            text-decoration: none;
        }

        .pagination p .activePage a {
            color: #8B0000;
        }

        .pagination p .noActiveEl{
            color: #828178;
        }
    </style>
</head>
<body>
<a href="../../index.jsp">Back to main menu</a>

<br/>
<br/>

<h1>User list</h1>

<c:url var="getAllUsersAction" value="/users"/>

<table border = "0">
    <tr>
        <td><b>User name</b></td>
            <form name = "listUsr" action="${getAllUsersAction}" method="get">
                <td>
                    <input type = "text" name = "userName" value = "${userName}" size = "25"/>
                </td>
                <td ><input type = "submit" value = "Find"/></td>
            </form>
            <form action="${getAllUsersAction}">
                <td>
                    <input type = "hidden" name = "page" value = "${page}"/>
                    <button type="submit">Clear</button>
                </td>
            </form>
    </tr>
</table>


    <table class="tg">
        <tr>
            <th width="20">ID</th>
            <th width="200">Name</th>
            <th width="30">Age</th>
            <th width="5">Admin</th>
            <th width="120">Create date</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach var="line" begin="0" end="${linebypage-1}">
            <c:if test="${listUsers.size()>line}">
                <c:set var="usr" value="${listUsers.get(line)}"/>
                <tr>
                    <td>${usr.id}</td>
                    <td>${usr.name}</td>
                    <td>${usr.age}</td>
                    <td>${usr.admin}</td>
                    <td>${usr.createDate}</td>
                    <td><a href="<c:url value='/edit?id=${usr.id}&page=${page}&userName=${userName}'/>">Edit</a></td>
                    <td><a href="<c:url value='/remove?id=${usr.id}&page=${page}&userName=${userName}'/>">Delete</a></td>
                </tr>
            </c:if>
            <c:if test="${listUsers.size()<=line}">
                <tr>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </c:if>
        </c:forEach>
    </table>

    <div class="pagination">
        <p>
                <c:if test="${page==1}">
                    <span class="noActiveEl">prew .. </span>
                </c:if>
                <c:if test="${page>1}">
                    <a href="<c:url value="/users" >
                        <c:param name="page" value="${page-1}"/>${page-1}
                        <c:param name="userName" value="${userName}"/>${userName}
                        </c:url>">
                        prew ..
                    </a>
                </c:if>
                <c:forEach begin="${startPage}" end="${startPage+4}" var="p">
                    <c:if test="${p==page}">
                        <span class="activePage">
                    </c:if>
                    <c:if test="${p<=endPage}">
                        <a href="<c:url value="/users" >
                            <c:param name="page" value="${p}"/>${p}
                            <c:param name="userName" value="${userName}"/>${userName}
                            </c:url>"> ${p} </a>
                    </c:if>
                    <c:if test="${p>endPage}">
                        <span class="noActiveEl"> ${p} </span>
                    </c:if>
                    <c:if test="${p==page}">
                        </span>
                    </c:if>

                </c:forEach>
                <c:if test="${page==lastPage}">
                    <span class="noActiveEl"> .. next</span>
                </c:if>
                <c:if test="${page<lastPage}">
                    <a href="<c:url value="/users" >
                        <c:param name="page" value="${page+1}"/>${page+1}
                            <c:param name="userName" value="${userName}"/>${userName}
                            </c:url>">
                        .. next
                    </a>
                </c:if>
        </p>
    </div>



<h1>Add / Edit a User</h1>

<c:url var="addAction" value="/users/add"/>

<form:form action="${addAction}" commandName="user">
    <input type="hidden" name = "page" value="${page}"/>
    <input type="hidden" name = "userName" value="${userName}"/>
    <table>
        <c:if test="${!empty user.name}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="name">
                    <spring:message text="Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="name"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="age">
                    <spring:message text="Age"/>
                </form:label>
            </td>
            <td>
                <form:input path="age"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="admin">
                    <spring:message text="Admin"/>
                </form:label>
            </td>
            <td>
                <form:select path="admin">
                    <form:option value="false">false</form:option>
                    <form:option value="true">true</form:option>
                </form:select>
            </td>
        </tr>
        <c:if test="${!empty user.name}">
            <tr>
                <td>
                    <form:label path="createDate">
                        <spring:message text="Create date"/>
                    </form:label>
                </td>
                <td>
                    <form:input type = "date" path="createDate" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="createDate"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td colspan="2">
                <c:if test="${!empty user.name}">
                    <input type="submit"
                           value="<spring:message text="Save"/>"/>
                </c:if>
                <c:if test="${empty user.name}">
                    <input type="submit"
                           value="<spring:message text="Add User"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>

</body>
</html>
