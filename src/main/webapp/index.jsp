<%--
  Created by IntelliJ IDEA.
  User: andrey
  Date: 17.11.16
  Time: 12.08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Editor</title>
</head>
<body>
<h2>Test task for JavaRush (CRUD)</h2>
<h3>User editor</h3>
<br/>
<a href="<c:url value="/users"/>" target="_self">Click to edit "User list"</a>
<br/>
</body>
</html>