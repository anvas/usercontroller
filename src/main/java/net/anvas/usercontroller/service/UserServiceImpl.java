package net.anvas.usercontroller.service;

import net.anvas.usercontroller.dao.UserDao;
import net.anvas.usercontroller.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by andrey on 16.11.16.
 */
@Service
public class UserServiceImpl implements UserService{
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public void addUser(User user) {
        this.userDao.addUser(user);

    }

    @Override
    @Transactional
    public void updateUser(User user) {
        this.userDao.updateUser(user);
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        this.userDao.removeUser(id);
    }

    @Override
    @Transactional
    public User getUserById(int id) {
        return this.userDao.getUserById(id);
    }

    @Override
    @Transactional
    public List<User> getAllUsers(String filter, int page, int lineByPage) {
        return this.userDao.getAllUsers(filter, page, lineByPage);
    }

    @Override
    @Transactional
    public Integer getUsersCount(String filter) {
        return this.userDao.getUsersCount(filter);
    }


}
