package net.anvas.usercontroller.dao;

import net.anvas.usercontroller.model.User;

import java.util.List;

/**
 * Created by andrey on 16.11.16.
 */
public interface UserDao {

    public void addUser(User user);

    public void updateUser(User user);

    public  void removeUser(int id);

    public User getUserById(int id);

    public List<User> getAllUsers(String filter, int page, int lineByPage);

    public Integer getUsersCount(String filter);

}
