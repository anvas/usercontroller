package net.anvas.usercontroller.controller;

import net.anvas.usercontroller.model.User;
import net.anvas.usercontroller.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by andrey on 16.11.16.
 */
@Controller
public class UserController {
    private static final int LINE_BY_PAGE=10;

    private UserService userService;

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String getAllUsers(@RequestParam(value = "page", required = false) Integer page,
                              @RequestParam(value = "userName", required = false) String userName, Model model){
        userName = userName == null ? "" : userName;
        Integer lastPage = (this.userService.getUsersCount(userName)-1) / LINE_BY_PAGE + 1;
        page = page ==null ? 1 : page > lastPage ? lastPage : page;
        Integer startPage = page-2 > 0 ? page-2:1;
        Integer endPage = startPage+4 < lastPage ? startPage+4:lastPage;
        startPage = endPage-4 > 0 ? endPage-4:1;
        model.addAttribute("linebypage", LINE_BY_PAGE);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("page", page);
        model.addAttribute("userName", userName);
        model.addAttribute("user", new User());
        String filter="";
        if (!userName.equals("")){
            filter="where name like '%"+userName+"%'";
        }
        model.addAttribute("listUsers", this.userService.getAllUsers(filter, page-1, LINE_BY_PAGE));

        return "users";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") @DateTimeFormat(pattern = "yyyy-MM-dd") User user,
                          @RequestParam(value = "page", required = false) Integer page,
                          @RequestParam(value = "userName", required = false) String userName){
        userName = userName == null ? "" : userName;
        Integer lastPage = (this.userService.getUsersCount(userName)-1) / LINE_BY_PAGE + 1;
        page = page ==null ? 1 : page > lastPage ? lastPage : page;
        if(user.getId() == 0){
            user.setCreateDate(new Date());
            this.userService.addUser(user);
        }else  {
            this.userService.updateUser(user);
        }

        return "redirect:/users?page="+page+"&userName="+userName;
    }

    @RequestMapping("/remove")
    public String removeUser(@RequestParam(value = "id") int id,
                             @RequestParam(value = "page", required = false) Integer page,
                             @RequestParam(value = "userName", required = false) String userName){
        userName = userName == null ? "" : userName;
        Integer lastPage = (this.userService.getUsersCount(userName)-1) / LINE_BY_PAGE + 1;
        page = page ==null ? 1 : page > lastPage ? lastPage : page;
        this.userService.removeUser(id);

        return "redirect:/users?page="+page+"&userName="+userName;
    }

    @RequestMapping("/edit")
    public String editUser(@RequestParam(value = "id") int id,
                           @RequestParam(value = "page", required = false) Integer page,
                           @RequestParam(value = "userName", required = false) String userName, Model model) {
        userName = userName == null ? "" : userName;
        int lastPage = (this.userService.getUsersCount(userName)-1) / LINE_BY_PAGE + 1;
        page = page == null ? 1 : page > lastPage ? lastPage : page;
        int startPage = page-2 > 0 ? page-2:1;
        int endPage = startPage+4 < lastPage ? startPage+4:lastPage;
        startPage = endPage-4 > 0 ? endPage-4:1;
        model.addAttribute("linebypage", LINE_BY_PAGE);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("page", page);
        model.addAttribute("userName", userName);
        model.addAttribute("user", this.userService.getUserById(id));
        String filter="";
        if (!userName.equals("")){
            filter="where name like '%"+userName+"%'";
        }
        model.addAttribute("listUsers", this.userService.getAllUsers(filter, page-1,LINE_BY_PAGE));

        return "users";
    }
}
