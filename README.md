# README #

Тестовое задание для участия в проекте JavaRush (CRUD-Users)

1. Проект собирается без дополнительных танцев с бубном.
2. Схема БД: test, Пользователь: root, пароль: root
3. В корне проекта файл CreateData.sql - скрипт для создания схемы БД, таблицы User и наполнения ее данными.
4. Главная страница:http://localhost:8080/index.jsp. На ней единственная ссылка для перехода к основной странице: http://localhost:8080/users
5. Поиск осуществляется только по всем вхождениям поля name. Для отмены поиска и вывода полного списка необходимо нажать кнопку "clear"
6. Удаление по ссылке "delete" в строке с пользователем.
7. Редактирование в форме снизу страницы по кнопке "edit"
8. Поле createData не редактируется. Заполняется автоматически при записи нового User.
9. У меня работало под TomCat 8.0.39