CREATE SCHEMA `test` ;
CREATE TABLE `test`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(25) NOT NULL,
  `age` INT NOT NULL,
  `isAdmin` BIT NOT NULL,
  `createDate` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Ivanov Ivan',
22,
1);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Agatha MacDonald',
11,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Albert O’Connor',
12,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Adam Gordon',
13,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Amanda Brian',
14,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Alexander Chester',
15,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Andrew Grant',
16,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Anna Ross',
17,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Augusta Walter',
18,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Audrey Smith',
19,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Allan Butler',
20,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Alison Black',
21,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Barrett Nash-Williams',
22,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Bruce Robertson',
23,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Boris Jones',
24,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Carl Murphy',
25,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Carla Williams',
26,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Catherine Edwards',
27,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Cliff Birds',
28,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Curtis Cook',
29,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Claudia Sinclair',
30,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Constance London',
31,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Daisy Nail',
32,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Dexter Round',
33,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('James Allford',
34,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Gerald Robin',
35,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Frankie Walls',
36,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Elvin Thomas',
37,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Dianne Brook',
38,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Douglas Fairy',
39,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Deborah Waite',
40,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Kent Peacock',
22,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Kimberly Mason',
23,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Joanna Kingsman',
24,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Richard Hardman',
25,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Ophelia Stevenson',
26,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Norman Young',
27,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Matthew Turner',
28,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Nicole WilKinson',
29,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Theresa Holiday',
32,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Scott Bush',
42,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Sarah Blare',
52,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Roger Gill',
62,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Pamela Fletcher',
72,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Thomas Little',
82,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Stacey Longman',
22,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Stefania Backer',
23,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Louis Sheldon',
24,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('',
25,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Wilma Leapman',
26,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('William Jacobson',
27,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Vivien Archibald',
28,
0);

INSERT INTO `test`.`User`
(`name`,
`age`,
`isAdmin`)
VALUES
('Victor Bishop',
29,
0);